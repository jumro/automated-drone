#include <Arduino.h>
#include <Servo.h>

int speed = 0;
int incoming_byte = 0;
Servo rotor_fl;

void setup() {
  rotor_fl.attach(9);
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {    
    incoming_byte = Serial.read(); 

    // KeyBoard numbers from 1 until 9
    if(incoming_byte >= 49 && incoming_byte <= 57)
    {
      int inscoming_number = incoming_byte - 48;
      speed = map(inscoming_number, 1, 9, 0, 180); 
      rotor_fl.write(speed);
      Serial.print("Set Speed to: ");
      Serial.println(speed);
    }
  }
}